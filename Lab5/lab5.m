[p0, p1, p2, p3, p4, p6, p9] = get_digits();

% ellman()
% hopfield(p2, p4, p1)
hamming(p0, p1, p2, p3, p4, p6, p9)

function [] = ellman()
    fprintf('1. Elman network\n')
    %1.1 Создание обучающего множества
    % Основной сигнал
    k1 = 0:0.025:1;
    p1 = sin(4 * pi * k1);
    t1 = -ones(size(p1));
    % Сигнал, подледащий распознаванию
    a = 2.38;
    b = 4.1;
    k2 = a:0.025:b;
    p2 = cos(cos(k2) .* k2.^2 + 5*k2);
    t2 = +ones(size(p2));
    % Длительность сигнала
    R = {1;3;5};
    P = [repmat(p1, 1, R{1}), p2, repmat(p1, 1, R{2}), p2, repmat(p1, 1, R{3}), p2];
    T = [repmat(t1, 1, R{1}), t2, repmat(t1, 1, R{2}), t2, repmat(t1, 1, R{3}), t2];
    P = con2seq(P);
    T = con2seq(T);
    
    % 1.2 Создание сети
    net = layrecnet(1 : 2, 100, 'trainoss');
    net.layers{1}.transferFcn = 'tansig';
    net.layers{2}.transferFcn = 'tansig';
    net = configure(net, P, T);
    
    % 1.3
    [p, Xi, Ai, t] = preparets(net, P, T);
    
    %1.4
    net.trainParam.epochs = 100;
    net.trainParam.goal = 1.0e-5;
    
    %1.5
    net = train(net, p, t, Xi, Ai);
    Y = sim(net, p, Xi, Ai);
    
    %1.6
    view(net)
    
    %1.7
    figure();
    hold on;

    rLine = plot(cell2mat(Y), 'r');
    pLine = plot(cell2mat(T), 'b');
    legend([pLine,rLine],'Target', 'Predicted');
    hold off;
    
    %1.8
    t_pred = zeros(0, length(Y));
    right = 0;
    for i = 1:length(Y)
        if Y{i} >= 0
            t_pred(i) = 1;
        else
            t_pred(i) = -1;
        end
        
        if t_pred(i) == t{i}
            right = right + 1;
        end
    end
    fprintf("\t[1.8] Accuracy score for train set: %f\n", right/length(Y));
    
    %1.9
    R = {1;4;5};
    P = [repmat(p1, 1, R{1}), p2, repmat(p1, 1, R{2}), p2, repmat(p1, 1, R{3}), p2];
    T = [repmat(t1, 1, R{1}), t2, repmat(t1, 1, R{2}), t2, repmat(t1, 1, R{3}), t2];
    P = con2seq(P);
    T = con2seq(T);    
end

function [] = hopfield(p2, p4, p1)
    iterations = 600;

    %2.1 Создание сети
    P = [p2(:), p4(:), p1(:)];
    net = newhop(P);
    
    %2.2
    R = sim(net, {1 iterations}, {}, p2(:));
    draw_image(reshape(R{iterations}, 12, 10));
    
    %2.3
    noise_p4 = make_noise(p4, 0.2);
    draw_image(noise_p4);
    R = sim(net, {1 iterations}, {}, noise_p4(:));
    draw_image(reshape(R{iterations}, 12, 10));
    
    %2.4
    noise_p1 = make_noise(p1, 0.3);
    draw_image(noise_p1);
    R = sim(net, {1 iterations}, {}, noise_p1(:));
    draw_image(reshape(R{iterations}, 12, 10));
end

function [] = hamming(p0, p1, p2, p3, p4, p6, p9)
    P = [p0(:), p1(:), p2(:), p3(:), p4(:), p6(:), p9(:)];
    % 3.1/3.2
    Q = 7;
    eps = 1 / (Q - 1);
    R = 10*12;
    IW = [p0(:)'; p1(:)'; p2(:)'; p3(:)'; p4(:)'; p6(:)'; p9(:)'];
    b = ones(Q, 1) * R;
    a = zeros(Q, Q);
    for i = 1:Q
        a(:,i) = IW * P(:, i) + b;
    end
    % 3.3
    net = newhop(a);
    net.biasConnect(1) = 0;
    net.layers{1}.transferFcn = 'poslin';

    net.LW{1, 1} = eye(Q, Q) * (1 + eps) - ones(Q, Q) * eps;
    view(net);
    
    % 3.4
    iterations = 600;
    A1 = IW * p2(:) + b;
    R = sim(net, {1 iterations}, {}, A1);
    A2 = R{iterations};
    A = get_model(IW, A2);
    draw_image(reshape(A, 12, 10))
    
    %3.5
    noise_p4 = make_noise(p4, 0.2);
    draw_image(noise_p4)
    A1 = IW * noise_p4(:) + b;
    R = sim(net, {1 iterations}, {}, A1);
    A2 = R{iterations};
    A = get_model(IW, A2);
    draw_image(reshape(A, 12, 10))
    
    %3.6
    noise_p1 = make_noise(p1, 0.3);
    draw_image(noise_p1)
    A1 = IW * noise_p1(:) + b;
    R = sim(net, {1 iterations}, {}, A1);
    A2 = R{iterations};
    A = get_model(IW, A2);
    draw_image(reshape(A, 12, 10))
end

function [] = draw_image(R)
    fig = figure();
    R(R >= 0) = 2;
    R(R < 1) = 1;
    map = [1, 1, 1; 0, 0, 0];
    image(R);
    colormap(map);
    axis off
    axis image
end

function [noise_p] = make_noise(p, noise_ration)
    r = rand([12,10]);
    noise_p = p;
    for i=1:12
        for j = 1:10
            if r(i,j) < noise_ration
                noise_p(i,j) = -noise_p(i,j);
            end
        end
    end
end

function [model] = get_model(IW, A)
    model = IW(A == max(A), :)';
end

function [p0, p1, p2, p3, p4, p6, p9] = get_digits()
p9 =      [-1 -1 -1 -1 +1 +1 +1 +1 +1 +1;
           -1 -1 -1 -1 +1 +1 +1 +1 +1 +1;
           -1 -1 -1 -1 +1 +1 -1 -1 +1 +1;
           -1 -1 -1 -1 +1 +1 -1 -1 +1 +1;
           -1 -1 -1 -1 +1 +1 -1 -1 +1 +1;
           -1 -1 -1 -1 +1 +1 -1 -1 +1 +1;
           -1 -1 -1 -1 +1 +1 +1 +1 +1 +1;
           -1 -1 -1 -1 +1 +1 +1 +1 +1 +1;
           -1 -1 -1 -1 -1 -1 -1 -1 +1 +1;
           -1 -1 -1 -1 -1 -1 -1 -1 +1 +1;
           -1 -1 -1 -1 +1 +1 +1 +1 +1 +1;
           -1 -1 -1 -1 +1 +1 +1 +1 +1 +1];
     
p0 =      [-1 -1 -1 -1 -1 -1 -1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 +1 +1 +1 +1 +1 +1 -1 -1;
           -1 +1 +1 +1 -1 -1 +1 +1 +1 -1;
           -1 +1 +1 +1 -1 -1 +1 +1 +1 -1;
           -1 +1 +1 +1 -1 -1 +1 +1 +1 -1;
           -1 +1 +1 +1 -1 -1 +1 +1 +1 -1;
           -1 +1 +1 +1 -1 -1 +1 +1 +1 -1;
           -1 +1 +1 +1 -1 -1 +1 +1 +1 -1;
           -1 -1 +1 +1 +1 +1 +1 +1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 -1 -1 -1 -1 -1 -1 -1];
       
p4 =      [-1 +1 +1 -1 -1 -1 -1 +1 +1 -1;
           -1 +1 +1 -1 -1 -1 -1 +1 +1 -1;
           -1 +1 +1 -1 -1 -1 -1 +1 +1 -1;
           -1 +1 +1 -1 -1 -1 -1 +1 +1 -1;
           -1 +1 +1 -1 -1 -1 -1 +1 +1 -1;
           -1 +1 +1 +1 +1 +1 +1 +1 +1 -1;
           -1 +1 +1 +1 +1 +1 +1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1];

p1 =      [-1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1;
           -1 -1 -1 +1 +1 +1 +1 -1 -1 -1];
       
p2 =      [+1 +1 +1 +1 +1 +1 +1 +1 -1 -1;
           +1 +1 +1 +1 +1 +1 +1 +1 -1 -1;
           -1 -1 -1 -1 -1 -1 +1 +1 -1 -1;
           -1 -1 -1 -1 -1 -1 +1 +1 -1 -1;
           -1 -1 -1 -1 -1 -1 +1 +1 -1 -1;
           +1 +1 +1 +1 +1 +1 +1 +1 -1 -1;
           +1 +1 +1 +1 +1 +1 +1 +1 -1 -1;
           +1 +1 -1 -1 -1 -1 -1 -1 -1 -1;
           +1 +1 -1 -1 -1 -1 -1 -1 -1 -1;
           +1 +1 -1 -1 -1 -1 -1 -1 -1 -1;
           +1 +1 +1 +1 +1 +1 +1 +1 -1 -1;
           +1 +1 +1 +1 +1 +1 +1 +1 -1 -1];
       
p3 =      [-1 -1 +1 +1 +1 +1 +1 +1 -1 -1;
           -1 -1 +1 +1 +1 +1 +1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 +1 +1 +1 +1 -1 -1;
           -1 -1 -1 -1 +1 +1 +1 +1 -1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 -1 -1 -1 -1 -1 +1 +1 -1;
           -1 -1 +1 +1 +1 +1 +1 +1 +1 -1;
           -1 -1 +1 +1 +1 +1 +1 +1 -1 -1];

p6 =      [+1 +1 +1 +1 +1 +1 -1 -1 -1 -1;
           +1 +1 +1 +1 +1 +1 -1 -1 -1 -1;
           +1 +1 -1 -1 -1 -1 -1 -1 -1 -1;
           +1 +1 -1 -1 -1 -1 -1 -1 -1 -1;
           +1 +1 +1 +1 +1 +1 -1 -1 -1 -1;
           +1 +1 +1 +1 +1 +1 -1 -1 -1 -1;
           +1 +1 -1 -1 +1 +1 -1 -1 -1 -1;
           +1 +1 -1 -1 +1 +1 -1 -1 -1 -1;
           +1 +1 -1 -1 +1 +1 -1 -1 -1 -1;
           +1 +1 -1 -1 +1 +1 -1 -1 -1 -1;
           +1 +1 +1 +1 +1 +1 -1 -1 -1 -1;
           +1 +1 +1 +1 +1 +1 -1 -1 -1 -1];
end