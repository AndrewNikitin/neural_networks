% Лабораторная работа №4 "Сети с радиальными базисными элементами"

% Использовать вероятностную нейронную сеть для классификации точек в 
% случае, когдаклассы не являются линейно разделимыми
% stage1();
% Использовать сеть с радиальными базисными элементами (RBF) для 
% классификации точек в случае, когда классы не являются линейно разделимыми.
% stage2();
% Использовать обобщенно-регрессионную нейронную сеть для аппроксимации 
% функции. Проверить работу сети с рыхлыми данными.
stage3();

function [] = stage1()
    % 1.1
    [e1, e2, e3] = get_ellipses();
    [x1, x2, x3] = get_training_set(e1, 60, e2, 100, e3, 120);
    [t1, t2, t3] = get_class_labels(60, 100, 120);
    
    % 1.2
    [trainInd1, testInd1] = dividerand(60, 0.8, 0.2);
    [trainInd2, testInd2] = dividerand(100, 0.8, 0.2);
    [trainInd3, testInd3] = dividerand(120, 0.8, 0.2);
    
    % 1.3
    plot_train_test_set(e1, x1, trainInd1, testInd1, ...
                        e2, x2, trainInd2, testInd2, ...
                        e3, x3, trainInd3, testInd3)
                    
   % 1.4
   trainInd = cat(2, trainInd1, trainInd2 + 60, trainInd3 + 160);
   testInd = cat(2, testInd1, testInd2 + 60, testInd3 + 160);
   x = cat(2, x1, x2, x3);
   t = cat(2, t1, t2, t3);
   
   % 1.5
   % ???
   
   % 1.6
   spread = 0.3;
   net = newpnn(x(:, trainInd), t(:,trainInd), spread);
   
   % 1.7
   view(net);
   
   % 1.8
   y_pred = net(x(:, trainInd));
   y_true = t(:, trainInd);
   fprintf('Размер обучающей выборки: %d\n',length(trainInd));
   fprintf('Количество совпадений: %d\n\n',sum(y_true(y_true == y_pred)));
   
   % 1.9
   y_pred = net(x(:, testInd));
   y_true = t(:, testInd);
   fprintf('Размер тестовой выборки: %d\n',length(testInd));
   fprintf('Количество совпадений: %d\n\n',sum(y_true(y_true == y_pred)));
   
   % 1.10
   [gX, gY] = meshgrid(-1.2:0.025:1.2, 1.2:-0.025:-1.2);

   A = net([gX(:)';gY(:)']);
   n = length(gX);
   A = max(0, min(1, A));
   A = round(A * 10) * 0.1;

   ctable = unique(A', 'rows');
   cmap = zeros(n, n);

   for i = 1 : size(ctable, 1)
       cmap(ismember(A', ctable(i, :), 'rows')) = i; 
   end
   figure()
   hold on
   image([-1.2, 1.2], [-1.2, 1.2], cmap); 
   colormap(ctable);
   axis([-1.2 1.2 -1.2 1.2]);
   hold off;
   grid on;
end

function [] = stage2()
    % 2.1
    [e1, e2, e3] = get_ellipses();
    [x1, x2, x3] = get_training_set(e1, 60, e2, 100, e3, 120);
    [t1, t2, t3] = get_class_labels(60, 100, 120);
    
    % 2.2
    [trainInd1, testInd1] = dividerand(60, 0.8, 0.2);
    [trainInd2, testInd2] = dividerand(100, 0.8, 0.2);
    [trainInd3, testInd3] = dividerand(120, 0.8, 0.2);
    
    % 2.3
    plot_train_test_set(e1, x1, trainInd1, testInd1, ...
                        e2, x2, trainInd2, testInd2, ...
                        e3, x3, trainInd3, testInd3)
                    
    % 2.4
    trainInd = cat(2, trainInd1, trainInd2 + 60, trainInd3 + 160);
    testInd = cat(2, testInd1, testInd2 + 60, testInd3 + 160);
    x = cat(2, x1, x2, x3);
    t = cat(2, t1, t2, t3);
    
    % 2.5
    spread = 0.1;
    goal = 2.0e-2;
    net = newrb(x(:, trainInd), t(:,trainInd), goal, spread);
    
    % 2.6
    view(net);
    
    % 2.7
    y_pred = net(x(:, trainInd));
    for j = 1:size(y_pred, 2)
        for i = 1:3
            if y_pred(i, j) < 0.5
                y_pred(i, j) = 0;
            else
                y_pred(i, j) = 1;
            end
        end
    end
    y_true = t(:, trainInd);
    fprintf('Размер обучающей выборки: %d\n',length(trainInd));
    fprintf('Количество совпадений: %d\n\n',sum(y_true(y_true == y_pred)));
    
    % 2.8
    y_pred = net(x(:, testInd));
    for j = 1:size(y_pred, 2)
        for i = 1:3
            if y_pred(i, j) < 0.5
                y_pred(i, j) = 0;
            else
                y_pred(i, j) = 1;
            end
        end
    end
    y_true = t(:, testInd);
    fprintf('Размер тренировочной выборки: %d\n',length(testInd));
    fprintf('Количество совпадений: %d\n\n',sum(y_true(y_true == y_pred)));
   
    % 2.9
    % 2.10
    % 2.11
    figure
    hold on;
    axis([-1.2 1.2 -1.2 1.2]);
    grid on

    [gX, gY] = meshgrid(-1.2:0.025:1.2, -1.2:0.025:1.2);

    A = net([gX(:)';gY(:)']);    
    A = max(0, min(1, A));
    A = round(A * 10) * 0.1;
    n = length(gX);

    ctable = unique(A', 'rows');
    cmap = zeros(n, n);

    for i = 1 : size(ctable, 1)
        cmap(ismember(A', ctable(i, :), 'rows')) = i; 
    end
    image([-1.2, 1.2], [-1.2, 1.2], cmap); 
    colormap(ctable);
end

function [] = stage3()
    % x(t) = sin(-2 * t^2 + 7 * t) | t \in [0, 3.5], h = 0.1
    h = 0.1;
    t = 0:h:3.5;
    x = sin(-2 * t.^2 + 7 * t);
    
    trainInd = 1:9*fix(length(t)/10);
    testInd = 9*fix(length(t)/10):length(t);
    
    % 3.1; 3.2
    net = newgrnn(t(trainInd), x(trainInd), h);
    
    % 3.4
    view(net);
    
    % 3.5
    y = sim(net, t(trainInd));
    plot_approximation_result(t(trainInd), x(trainInd), y);
    fprintf('-------------- 3.5 - 3.6 ------------------\n');
    fprintf('Ошибка на тренировочной выборке: %f\n',sqrt(mse(x(trainInd) - y)));
        
    % 3.6
    y = sim(net, t(testInd));
    plot_approximation_result(t(testInd), x(testInd), y);
    fprintf('Ошибка на тестовой выборке: %f\n',sqrt(mse(x(testInd) - y)));
    
    % 3.7
    [trainInd, testInd] = dividerand(length(t), 0.8, 0.2);
    net = newgrnn(t(trainInd), x(trainInd), h);
    y = sim(net, t);
    plot_approximation_result(t, x, y);
    fprintf('-------------- 3.7 - 3.8 ------------------\n');
    fprintf('Ошибка на тренировочной выборке: %f\n',sqrt(mse(x(trainInd) - y(trainInd))));
    fprintf('Ошибка на тестовой выборке: %f\n',sqrt(mse(x(testInd) - y(testInd))));
    
end

function [] = plot_approximation_result(t, x, y)
    figure();
    hold on;
    rLine = plot(t, x, 'r');
    aLine = plot(t, y, '-b');
    legend([rLine, aLine], 'Оригинал', 'Аппроксимация');
    hold off;
    grid on;
    
    figure();
    hold on;
    eLine = plot(t, abs(x - y), 'r');
    legend(eLine, 'Ошибка');
    hold off;
    grid on;
end

function [] = plot_train_test_set(e1, x1, trainInd1, testInd1, ...
                                  e2, x2, trainInd2, testInd2, ...
                                  e3, x3, trainInd3, testInd3)
    figure()
    hold on
    
    plot(e1(1, :), e1(2, :), '-r', 'Linewidth', 2)
    plot(e2(1, :), e2(2, :), '-g', 'Linewidth', 2)
    plot(e3(1, :), e3(2, :), '-b', 'Linewidth', 2)
    
    plot(x1(1, trainInd1), x1(2, trainInd1), 'or', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'r', ...
        'MarkerSize', 7);

    plot(x1(1, testInd1), x1(2, testInd1), 'rs', ...
         'MarkerEdgeColor', 'k', ...
         'MarkerFaceColor', 'm', ...
         'MarkerSize', 7);

    plot(x2(1, trainInd2), x2(2, trainInd2), 'og', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'g', ...
        'MarkerSize', 7);

    plot(x2(1, testInd2), x2(2, testInd2), 'gs', ...
         'MarkerEdgeColor', 'k', ...
         'MarkerFaceColor', 'm', ...
         'MarkerSize', 7);
     
     plot(x3(1, trainInd3), x3(2, trainInd3), 'ob', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'b', ...
        'MarkerSize', 7);

    plot(x3(1, testInd3), x3(2, testInd3), 'bs', ...
         'MarkerEdgeColor', 'k', ...
         'MarkerFaceColor', 'm', ...
         'MarkerSize', 7);
    
    hold off
    axis([-1.2 1.2 -1.2 1.2]);
    grid on;
end

function [t1, t2, t3] = get_class_labels(n1, n2, n3)
    t1 = repelem([1;0;0], 1, n1);
    t2 = repelem([0;1;0], 1, n2);
    t3 = repelem([0;0;1], 1, n3);
end

function [x1, x2, x3] = get_training_set(e1, n1, e2, n2, e3, n3)
    x1 = e1(:, randperm(size(e1, 2), n1));
    x2 = e2(:, randperm(size(e2, 2), n2));
    x3 = e3(:, randperm(size(e3, 2), n3));
end

function [e1, e2, e3] = get_ellipses()
    t = 0:0.025:2*pi;

    % Генерация точек первого класса
    e1 = [0.2 * cos(t) + (-0.25);
          0.2 * sin(t) + (+0.25)];
    % Генерация точек второго класса
    e2 = [0.7 * cos(t);
          0.5 * sin(t - pi/3)];
    % Генерация точек третьего класса
    e3 = [cos(t); sin(t)];
end