% Лабораторная работа №7 "Автоассоциативные сети с узким горлом"

% Использовать автоассоциативную сеть с узким горлом для отображения набора 
% данных,выделяя первую главную компоненту данных.
stage1()
% Использовать автоассоциативную сеть с узким горлом для аппроксимации 
% кривой на плоскости, выделяя первую нелинейную главную компоненту данных.
stage2()
% Применить автоассоциативную сеть с узким горлом для аппроксимации 
% пространственной кривой, выделяя старшие нелинейные главные компоненты 
% данных.
stage3()

function [] = stage1()
    % 1.1
    t = 0:0.025:2 * pi;
    x = [0.7 * cos(t) + 0;
         0.7 * sin(t - pi/6) + (-0.1)];
    xseq = con2seq(x);
    figure();
    plot(x(1, :), x(2, :), '-r', 'LineWidth', 2);
    
    % 1.2
    net = feedforwardnet(1, 'trainlm');
    net.layers{1}.transferFcn = 'purelin';
    net = configure(net, xseq, xseq);
    
    % 1.3
    net = init(net);
    
    % 1.4
    net.trainParam.epochs = 100;
    net.trainParam.goal = 1.0e-5;
    
    % 1.5
    net = train(net, xseq, xseq);
    
    % 1.6
    view(net);
    
    % 1.7
    yseq = sim(net, xseq);
    y = cell2mat(yseq);
    
    % 1.8
    figure();
    plot(x(1, :), x(2, :), '-r', y(1, :), y(2, :), '-b', 'LineWidth', 2);
end

function [] = stage2()
    % Вариант: r = 2*phi
    % 2.1
    phi = 0:0.025:2*pi;
    x = [2 * phi .* cos(phi);
         2 * phi .* sin(phi)];
    xseq = con2seq(x);
    figure()
    plot(x(1, :), x(2, :), '-r', 'LineWidth', 2);
    
    % 2.2
    net = feedforwardnet([10 1 10], 'trainlm');
    net.layers{1}.transferFcn = 'tansig';
    net.layers{2}.transferFcn = 'tansig';
    net.layers{3}.transferFcn = 'tansig';
    net = configure(net, xseq, xseq);
    
    % 2.3
    net = init(net);
    
    % 2.4
    net.trainParam.epochs = 300;
    net.trainParam.goal = 1.0e-5;
    
    % 2.5
    net = train(net, xseq, xseq);
    
    % 2.6
    view(net);
    
    % 2.7
    yseq = sim(net, xseq);
    y = cell2mat(yseq);
    
    % 2.8
    figure();
    plot(x(1, :), x(2, :), '-r', y(1, :), y(2, :), '-b', 'LineWidth', 2);
end

function [] = stage3()
    % 3.1
    phi = 0:0.025:2*pi;
    x = [2 * phi .* cos(phi);
         2 * phi .* sin(phi);
         phi];
    xseq = con2seq(x);
    figure()
    plot3(x(1, :), x(2, :), x(3, :), '-r', 'LineWidth', 2);
    
    % 3.2 
    net = feedforwardnet([10 2 10], 'trainlm');
    net.layers{1}.transferFcn = 'tansig';
    net.layers{2}.transferFcn = 'tansig';
    net.layers{3}.transferFcn = 'tansig';
    net = configure(net, xseq, xseq);
    
    % 3.3
    net = init(net);
    
     % 3.4
    net.trainParam.epochs = 100;
    net.trainParam.goal = 1.0e-5;
    
    % 3.5
    net = train(net, xseq, xseq);
    
    % 3.6
    view(net);
    
    % 3.7
    yseq = sim(net, xseq);
    y = cell2mat(yseq);
    
    % 3.8
    figure();
    plot3(x(1, :), x(2, :), x(3, :), '-r', ...
          y(1, :), y(2, :), y(3, :), '-b', ...
          'LineWidth', 2);
end