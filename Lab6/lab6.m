% Лабораторная работа №6 "Сети Кохонена"

stage1();
stage2();
stage3();
stage4();

function [] = stage1()
    bounds = [0 1.5; 0 1.5];
    clusters = 8;
    points = 10;
    std_dev = 0.1;
    X =  nngenc(bounds,clusters,points,std_dev);

    network = competlayer(8);
    network = configure(network, X);
    view(network);

    network.trainParam.epochs = 50;
    network = train(network, X);

    test = 1.5 * rand(2, 5);
    test_cl = vec2ind(sim(network, test));
    display(test)
    display(test_cl);

    figure();
    hold on;
    grid;
    scatter(X(1,:),X(2,:), 30, vec2ind(sim(network, X)), 'filled');
    scatter(test(1,:), test(2,:), 90, test_cl, 'o');
    title('Input Vectors');
    xlabel('x(1)');
    ylabel('x(2)');
    hold on;
    
end

function [] = stage2()
    bounds = [0 1.5; 0 1.5];
    clusters = 8;
    points = 10;
    std_dev = 0.1;
    X =  nngenc(bounds,clusters,points,std_dev);
    
    net = newsom(X, [2 4]);
    net.trainParam.epochs = 150;
    net = configure(net, X);
    net = train(net, X);
    view(net);

    test = 1.5 * rand(2, 5);
    test_cl = vec2ind(sim(net, test));
    display(test_cl);
    
    figure();
    hold on;
    grid;
    plotsom(net.IW{1}, net.layers{1}.distances);
    scatter(X(1,:),X(2,:), 30, vec2ind(sim(net, X)), 'filled');
    scatter(test(1,:), test(2,:), 90, test_cl, 'o');
    title('Input Vectors');
    xlabel('x(1)');
    ylabel('x(2)');
    hold on;
end

function [] = stage3()
    N = 20;
    X = 3 * rand(2, N) - 1.5;

    figure();
    hold on;
    grid on;
    plot(X(1,:), X(2,:), '-V', 'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'g', 'MarkerSize', 7);
    hold off;

    net = newsom(X, N);
    net = configure(net, X);
    net.trainParam.epochs = 600;
    net = train(net, X);
    view(net);

    figure;
    hold on;
    grid on;
    plotsom(net.IW{1}, net.layers{1}.distances);
    plot(X(1,:), X(2,:), 'V', 'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'g', 'MarkerSize', 7);
    hold off;
end

function [] = stage4()
    P = [0.6 0.2 1.2 0.9 0.2 -0.3 -1.1 -0.3 0.2 0.5 0.4 -1.3;
         -0.5 -1.2 1.1 -0.8 -1.5 -0.6 -1 -1.3 -0.1 0.5 -1.4 -0.6];
    T = [-1 1 -1 -1 1 -1 -1 -1 1 -1 1 -1];

    figure();
    hold on;
    grid on;
    plotpv(P, max(0, T));
    hold off;

    Ti = ind2vec(max(0, T) + 1);

    network = newlvq(minmax(P), 12);
    network = configure(network, P, Ti);
    network.trainParam.epochs = 300;

    network = train(network, P, Ti);
    view(network);

    [X,Y] = meshgrid(-1.5:0.1:1.5, -1.5:0.1:1.5);
    res = sim(network, [X(:)'; Y(:)']);
    res = vec2ind(res) - 1;

    figure();
    plotpv([X(:)'; Y(:)'], res);
    point = findobj(gca, 'type', 'line');
    set(point, 'Color', 'g');
    hold on;
    plotpv(P,  max(0, T));
    hold off;
end

function v = nngenc(x,c,n,d)
    r = size(x, 1);
    minv = min(x')';
    maxv = max(x')';
    v = rand(r,c) .* ((maxv-minv) * ones(1,c)) + (minv * ones(1,c));
    t = c*n;
    v = repmat(v,1,n) + randn(r,t)*d;
end