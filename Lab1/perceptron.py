from numpy.random import uniform
from numpy import vdot, zeros, array
from numpy.linalg import norm


class Perceptron(object):
	""" Класс, реализующий однослойный персептрон """

	def __init__(self, learning_rate=0.1):
		self.w = None
		self.b = None
		self.learning_rate = learning_rate

	def fit(self, data, labels, epochs=None, verbose=False):
		""" Обучение персептрона """
		samples = data.shape[0]
		dimension = data.shape[1]

		self.w = uniform(size=dimension)
		self.b = 1.0

		# if verbose:
		# 	print(f"Random initialization of weights:  w = {self.w}; b = {self.b}")

		current_epoch = 1
		while epochs is None or current_epoch <= epochs:
			new_w = self.w
			new_b = self.b
			acc_error = 0
			for i in range(samples):
				sample = data[i]
				label = labels[i]
				out = 1.0 if vdot(sample, new_w) + new_b >= 0 else 0.0
				new_w = new_w + self.learning_rate * (label - out) * sample
				new_b = new_b + self.learning_rate * (label - out)
				acc_error += abs(label - out)
			
			if verbose:
				print(f"Epoch: {current_epoch} | w = {self.w}; b = {self.b}; MAE = {acc_error / samples}")

			if acc_error / samples == 0:
				break
			else:
				self.w = new_w

			current_epoch += 1

	def predict_sample(self, sample):
		""" Определение класса для одного объекта """
		return 1.0 if vdot(sample, self.w) + self.b >= 0 else 0

	def predict(self, data):
		""" Определение класса для большого количества объектов """
		predicted = zeros(shape=data.shape[0])
		for i in range(data.shape[0]):
			predicted[i] = self.predict_sample(sample=data[i])
		return predicted

class MultiClassPerceptron(object):
	"""
	Класс, реализующий персептрон, разделяющий 2 ** outputs категорий
	"""
	def __init__(self, learning_rate=0.1, outputs=2):
		self.learning_rate = learning_rate
		self.outputs = outputs
		self.neurons = [Perceptron(self.learning_rate) for _ in range(outputs)]

	def fit(self, data, labels, epochs=50):
		""" Обучение """
		for p_num, p in enumerate(self.neurons):
			p.fit(data, labels[:, p_num], epochs)
		return self

	def predict_sample(self, sample):
		""" Определение класса для одного объекта """
		return array([p.predict_sample(sample) for p in self.neurons])

	def predict(self, data):
		""" Определение класса для большого количества объектов """
		predicted = zeros(shape=(data.shape[0], self.outputs))
		for i in range(data.shape[0]):
			predicted[i] = self.predict_sample(sample=data[i])
		return predicted