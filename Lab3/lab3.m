% Лабораторная работа №3 "Многослойные сети. Алгоритм обратного распространения ошибки"

stage1();
stage2();
stage3();


function [] = stage1()
    % 1.1
    [e1, e2, e3] = get_ellipses();
    [x1, x2, x3] = get_training_set(e1, 60, e2, 100, e3, 120);
    [t1, t2, t3] = get_class_labels(60, 100, 120);
    
    % 1.2
    [trainInd1, valInd1, testInd1] = dividerand(60, 0.7, 0.2, 0.1);
    [trainInd2, valInd2, testInd2] = dividerand(100, 0.7, 0.2, 0.1);
    [trainInd3, valInd3, testInd3] = dividerand(120, 0.7, 0.2, 0.1);
    
    % 1.3
    plot_train_test_set(e1, x1, trainInd1, valInd1, testInd1, ...
                        e2, x2, trainInd2, valInd2, testInd2, ...
                        e3, x3, trainInd3, valInd3, testInd3)
                    
    % 1.4
    trainInd = cat(2, trainInd1, trainInd2 + 60, trainInd3 + 160);
    testInd = cat(2, testInd1, testInd2 + 60, testInd3 + 160);
    valInd = cat(2, valInd1, valInd2 + 60, valInd3 + 160);
    x = cat(2, x1, x2, x3);
    t = cat(2, t1, t2, t3);
    
    % 1.5
    net = feedforwardnet(20);
    net = configure(net, [-1.2 1.2; 0 1]);
    net.layers{1}.transferFcn = 'tansig';
    net.trainFcn = 'trainrp';
    
    % 1.6
    net.divideFcn = 'divideind';
    
    net.divideParam.trainInd= trainInd;
    net.divideParam.valInd= valInd;
    net.divideParam.testInd= testInd;
    
    % 1.7
    net = init(net);
    
    % 1.8
    net.trainParam.epochs = 3000;
    net.trainParam.max_fail = 1500;
    net.trainParam.goal = 1e-5;
    
    % 1.9
    net = train(net, x, t);
    
    % 1.10
    
    % 1.11
    train_score = get_score(net, x(:, trainInd), t(:, trainInd));
    fprintf('Размер обучающей выборки: %d\n', length(trainInd));
    fprintf('Количество совпадений: %d\n\n', train_score);
    
    % 1.12
    val_score = get_score(net, x(:, valInd), t(:, valInd));
    fprintf('Размер валидационной выборки: %d\n', length(valInd));
    fprintf('Количество совпадений: %d\n\n', val_score);
    
    test_score = get_score(net, x(:, testInd), t(:, testInd));
    fprintf('Размер тестовой выборки: %d\n', length(testInd));
    fprintf('Количество совпадений: %d\n\n', test_score);
    
    % 1.13
    [gX, gY] = meshgrid(-1.2:0.025:1.2, 1.2:-0.025:-1.2);

    A = net([gX(:)';gY(:)']);
    n = length(gX);
    A = max(0, min(1, A));
    A = round(A * 10) * 0.1;

    ctable = unique(A', 'rows');
    cmap = zeros(n, n);

    for i = 1 : size(ctable, 1)
        cmap(ismember(A', ctable(i, :), 'rows')) = i; 
    end
    figure()
    hold on
    image([-1.2, 1.2], [-1.2, 1.2], cmap); 
    colormap(ctable);
    axis([-1.2 1.2 -1.2 1.2]);
    hold off;
    grid on;
end

function [] = stage2()
    h = 0.01;
    t = 0:h:3.5;
    x = sin(-2 * t.^2 + 7 * t);
    
    trainInd = 1:9*fix(length(t)/10);
    valInd = 9*fix(length(t)/10):length(t);
    testInd = [];
    
    % 2.1 
    net = feedforwardnet(20, 'traingdx');
    net.trainParam.lr = 0.05;
    net.trainParam.lr_inc = 1.05;
    
    net = configure(net, t, x);
    
    % 2.2
    net.divideFcn = 'divideind';
    
    net.divideParam.trainInd= trainInd;
    net.divideParam.valInd= valInd;
    net.divideParam.testInd= testInd;
    
    % 2.3
    net = init(net);
    
    % 2.4 
    net.trainParam.epochs = 1200;
    net.trainParam.max_fail = 600;
    net.trainParam.goal = 1e-8;
    
    % 2.5
    net = train(net, t, x);
    
    % 2.6
    % Отобразить структуру сети
    
    % 2.7
    x_pred = net(t);
    
    fprintf("MSE на обучающей выборке: %f\n", sqrt(mse(x(trainInd) - x_pred(trainInd))));
    fprintf("MSE на  валидационной выборке: %f\n", sqrt(mse(x(valInd) - x_pred(valInd))));
    
    figure;
    hold on;
    rLine = plot(t, x, '-b');
    aLine = plot(t, x_pred, '-r');
    legend([rLine, aLine], 'Оригинал', 'Аппроксимация');
    grid on;

    figure;
    eLine = plot(t, abs(x_pred -x));
    legend(eLine, 'Ошибка');
    grid on;
end

function [] = stage3()
    h = 0.01;
    t = 0:h:3.5;
    x = sin(-2 * t.^2 + 7 * t);
    
    trainInd = 1:9*fix(length(t)/10);
    valInd = 9*fix(length(t)/10):length(t);
    testInd = [];
    
    % 3.1 
    net = feedforwardnet([15 15], 'trainoss');
    net = configure(net, t, x);
    
    % 3.2
    net.divideFcn = 'divideind';
    
    net.divideParam.trainInd= trainInd;
    net.divideParam.valInd= valInd;
    net.divideParam.testInd= testInd;
    
    % 3.3
    net = init(net);
    
    % 3.4 
    net.trainParam.epochs = 3000;
    net.trainParam.max_fail = 600;
    net.trainParam.goal = 1e-8;
    
    % 3.5
    net = train(net, t, x);
    
    % 3.6
    % Отобразить структуру сети
    view(net)
    
    % 3.7
    x_pred = net(t);
    
    fprintf("MSE на обучающей выборке: %f\n", sqrt(mse(x(trainInd) - x_pred(trainInd))));
    fprintf("MSE на  валидационной выборке: %f\n", sqrt(mse(x(valInd) - x_pred(valInd))));
    
    figure;
    hold on;
    rLine = plot(t, x, '-b');
    aLine = plot(t, x_pred, '-r');
    legend([rLine, aLine], 'Оригинал', 'Аппроксимация');
    grid on;

    figure;
    eLine = plot(t, abs(x_pred -x));
    legend(eLine, 'Ошибка');
    grid on;
end

function [score] = get_score(net, x, t_true)
    t_pred = net(x);
    for j = 1:size(t_pred, 2)
        for i = 1:3
            if t_pred(i, j) < 0.5
                t_pred(i, j) = 0;
            else
                t_pred(i, j) = 1;
            end
        end
    end
    score = sum(t_true(t_true == t_pred));
end

function [] = plot_train_test_set(e1, x1, trainInd1, valInd1, testInd1, ...
    e2, x2, trainInd2, valInd2, testInd2, ...
    e3, x3, trainInd3, valInd3, testInd3)
    figure()
    hold on

    plot(e1(1, :), e1(2, :), '-r', 'Linewidth', 2)
    plot(e2(1, :), e2(2, :), '-g', 'Linewidth', 2)
    plot(e3(1, :), e3(2, :), '-b', 'Linewidth', 2)

    plot(x1(1, trainInd1), x1(2, trainInd1), 'or', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'r', ...
        'MarkerSize', 7);

    plot(x1(1, testInd1), x1(2, testInd1), 'rs', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'm', ...
        'MarkerSize', 7);

    plot(x1(1, valInd1), x1(2, valInd1), 'rv', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'c', ...
        'MarkerSize', 7);

    plot(x2(1, trainInd2), x2(2, trainInd2), 'og', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'g', ...
        'MarkerSize', 7);

    plot(x2(1, testInd2), x2(2, testInd2), 'gs', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'm', ...
        'MarkerSize', 7);
    
    plot(x2(1, valInd2), x2(2, valInd2), 'gv', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'c', ...
        'MarkerSize', 7);


    plot(x3(1, trainInd3), x3(2, trainInd3), 'ob', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'b', ...
        'MarkerSize', 7);

    plot(x3(1, testInd3), x3(2, testInd3), 'bs', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'm', ...
        'MarkerSize', 7);
    
    plot(x3(1, valInd3), x3(2, valInd3), 'bv', ...
        'MarkerEdgeColor', 'k', ...
        'MarkerFaceColor', 'c', ...
        'MarkerSize', 7);

    hold off
    axis([-1.2 1.2 -1.2 1.2]);
    grid on;
end

function [t1, t2, t3] = get_class_labels(n1, n2, n3)
    t1 = repelem([1;0;0], 1, n1);
    t2 = repelem([0;1;0], 1, n2);
    t3 = repelem([0;0;1], 1, n3);
end

function [x1, x2, x3] = get_training_set(e1, n1, e2, n2, e3, n3)
    x1 = e1(:, randperm(size(e1, 2), n1));
    x2 = e2(:, randperm(size(e2, 2), n2));
    x3 = e3(:, randperm(size(e3, 2), n3));
end

function [e1, e2, e3] = get_ellipses()
    t = 0:0.025:2*pi;

    % Генерация точек первого класса
    e1 = [0.2 * cos(t) + (-0.25);
          0.2 * sin(t) + (+0.25)];
    % Генерация точек второго класса
    e2 = [0.7 * cos(t);
          0.5 * sin(t - pi/3)];
    % Генерация точек третьего класса
    e3 = [cos(t); sin(t)];
end