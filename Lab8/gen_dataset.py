import csv

path_to_initial = './SN_m_tot_V2.0.csv'
start_date = 1830 * 100 + 4

needed_values = []

with open(path_to_initial) as fo:
    for line in fo:
        line = line.split(';')
        current_date = int(line[0]) * 100 + int(line[1])
        value = float(line[3])
        if current_date >= start_date:
            needed_values.append(value)


path_to_final = './data.txt'

with open(path_to_final, 'w') as myfile:
     for val in needed_values:
        myfile.write(str(val) + '\n')

