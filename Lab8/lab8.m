% Лабораторная работа №8 "Динамические сети"

% 1. Использовать сеть прямого распространения с запаздыванием для 
%   предсказания значенийвременного ряда и выполнения многошагового прогноза
stage1();
% 2. Использовать сеть сеть прямого распространения с распределенным 
%    запаздыванием дляраспознавания динамических образов.
stage2();
% 3. Использовать нелинейную авторегрессионную сеть с внешними входами для 
%    аппроксимации траектории динамической системы 
%    и выполнения многошагового прогноза.
stage3();

function [] = stage1()
    % 1.1
    % Файл загружен с http://sidc.oma.be/sunspot-data/
    % data.txt содержит необходимую часть данных
    
    % 1.2
    fileId = fopen('data.txt', 'r');
    x = fscanf(fileId, '%f %f', [1 inf]);
    
    % 1.3
    x = smooth(x, 12);
    
    % 1.4
    delays = 5;
    
    trainSize = 500;
    valSize = 100;
    testSize = 50;
    
    % 1.5
    trainInd = 1:trainSize;
    valInd = trainSize + 1: trainSize + valSize;
    testInd = trainSize + valSize + 1: trainSize + valSize + testSize;
    
    % 1.6
    x = con2seq(transpose(x(1:trainSize + valSize + testSize)));
    
    % 1.7
    net = timedelaynet(1:delays, 8, 'trainlm');
    
    % 1.8
    net.divideFcn = 'divideind';
    
    net.divideParam.trainInd= trainInd;
    net.divideParam.valInd= valInd;
    net.divideParam.testInd= testInd;
    
    % 1.9
    net = configure(net, x, x);
    
    % 1.10
    net = init(net);
    
    % 1.11
    net.trainParam.epochs = 600;
    net.trainParam.max_fail = 600;
    net.trainParam.goal = 1e-5;
    
    % 1.12
    [Xs, Xi, Ai, Ts] = preparets(net, x, x); 
    net = train(net, Xs, Ts, Xi, Ai);
    
    % 1.13
    view(net);
    
    % 1.14
    y = sim(net, Xs, Xi);
    
    y = cell2mat(y);
    x = cell2mat(x);
    xi = cell2mat(Xi);
    
    figure();
    hold on;
    rLine = plot(x, 'r');
    aLine = plot([xi y], 'b');
    hold off;
    grid on;
    legend([rLine aLine], 'Оригинал', 'Аппроксимация');
    
    figure();
    eLine = plot(abs(x - [xi y]), 'r');
    legend(eLine, 'Ошибка');
    
    % 1.15 https://www.mathworks.com/help/deeplearning/ug/multistep-neural-network-prediction.html
    netc = closeloop(net);
    view(netc)
    
    x = x(trainSize + valSize - 8: trainSize + valSize + testSize);
    x = con2seq(x);
    [xs,xi,ai,ts] = preparets(net,x,x);
    y = sim(netc,xs,xi);
    display(xi);
    display(x(1:length(xi)));
    
    y = cell2mat(y);
    x = cell2mat(x);
    xi = cell2mat(xi);
    
    figure();
    hold on;
    rLine = plot(x, 'r');
    aLine = plot([xi y], 'b');
    hold off;
    grid on;
    legend([rLine aLine], 'Оригинал', 'Аппроксимация');
    
    figure();
    eLine = plot(abs(x - [xi y]), 'r');
    legend(eLine, 'Ошибка');  
end

function [] = stage2()
    %2.1 Создание обучающего множества
    % Основной сигнал
    k1 = 0:0.025:1;
    p1 = sin(4 * pi * k1);
    t1 = -ones(size(p1));
    % Сигнал, подледащий распознаванию
    a = 2.38;
    b = 4.1;
    k2 = a:0.025:b;
    p2 = cos(cos(k2) .* k2.^2 + 5*k2);
    t2 = +ones(size(p2));
    % Длительность сигнала
    R = {1;3;5};
    P = [repmat(p1, 1, R{1}), p2, repmat(p1, 1, R{2}), p2, repmat(p1, 1, R{3}), p2];
    T = [repmat(t1, 1, R{1}), t2, repmat(t1, 1, R{2}), t2, repmat(t1, 1, R{3}), t2];
    P = con2seq(P);
    T = con2seq(T);
    
    % 2.2 
    net = distdelaynet({0 : 4, 0 : 4}, 8, 'trainoss');
    net.layers{1}.transferFcn = 'tansig';
    net.layers{2}.transferFcn = 'tansig';
    net.divideFcn = '';
    
    % 2.3
    net = configure(net, P, T);
    view(net);
    
    % 2.4
    [Xs, Xi, Ai, Ts] = preparets(net, P, T);
    
    % 2.5
    net.trainParam.epochs = 200;
    net.trainParam.goal =  1.0e-5;
    
    % 2.6, 2.7
    net = train(net, Xs, Ts, Xi, Ai);
    
    % 2.8
    Y = sim(net, Xs, Xi, Ai);
    
    figure();
    hold on;

    rLine = plot(cell2mat(Y), 'r');
    pLine = plot(cell2mat(T), 'b');
    legend([pLine,rLine],'Target', 'Predicted');
    hold off;
    
    % 2.9
    t_orig = cell2mat(T);
    t_pred = cell2mat(Y);
    
    for i = 1:size(t_orig, 2)
        if t_pred(i) < 0
            t_pred(i) = -1;
        else
            t_pred(i) = +1;
        end
    end
    display(t_pred);
end

function [] = stage3()
    k = 0:0.01:10;
    u = sin(k.^2 - 2 * k + 3);
    y = zeros(1, length(k));
    n = length(k);
    for i = 2:n
        y(i) = y(i - 1) / (1 + y(i - 1)^2) + u(i - 1)^3;
    end
    
    figure;
    subplot(2, 1, 1);
    hold on;
    grid;
    control = plot(k, u, '-b');
    legend(control, 'Control');
    hold off;
    subplot(2, 1, 2);
    hold on;
    grid;
    state = plot(k, y, '-r');
    legend(state, 'State');
    hold off;
    
    D = 3;
    ntrain = 700;
    nval = 200;
    ntest = 97;
    
    trainInd = 1 : ntrain;
    valInd = ntrain + 1 : ntrain + nval;
    testInd = ntrain + nval + 1 : ntrain + nval + ntest;
    
    net = narxnet(1 : D, 1 : D, 8);
    net.trainFcn = 'trainlm';
    
    net.divideFcn = 'divideind';
    net.divideParam.trainInd = trainInd;
    net.divideParam.valInd = valInd;
    net.divideParam.testInd = testInd;
    
    net.trainParam.epochs = 2000;
    net.trainParam.max_fail = 2000;
    net.trainParam.goal = 1.0e-5;
    
    [Ys, Yi, Ai, Ts] = preparets(net, con2seq(u), {}, con2seq(y));
    
    net = train(net, Ys, Ts, Yi, Ai);
    view(net);
    
    y_pred = sim(net, Ys, Yi);
    
    figure;
    subplot(3,1,1);
    hold on;
    grid;
    control = plot(k, u, '-b');
    legend(control, 'Control');
    hold off;
    subplot(3,1,2);
    hold on;
    grid;
    orig = plot(k, y, '-b');
    pred = plot(k, [y(1:D) cell2mat(y_pred)], '-r');
    legend([orig, pred], 'Original', 'Predicted');
    hold off;
    subplot(3,1,3);
    hold on;
    grid;
    error = plot(k(4:n), y(D+1:n) - cell2mat(y_pred), '-r');
    legend(error, 'Error');
    hold off;
    k = k(ntrain + nval - D + 1: ntrain + nval + ntest);
    n = length(k);
    y = y(ntrain + nval - D + 1: ntrain + nval + ntest);
    u = u(ntrain + nval - D + 1: ntrain + nval + ntest);
    netc = closeloop(net);
    [Ys, Yi, Ai, Ts] = preparets(netc, con2seq(u), {}, con2seq(y));
    display(netc.numInputs);
    y_pred = netc(Ys, Yi);
    
    display(k);
    
    figure;
    subplot(3,1,1);
    hold on;
    grid;
    control = plot(k, u, '-b');
    legend(control, 'Control');
    hold off;
    subplot(3,1,2);
    hold on;
    grid;
    orig = plot(k, y, '-b');
    pred = plot(k, [y(1:D) cell2mat(y_pred)], '-r');
    legend([orig, pred], 'Original', 'Predicted');
    hold off;
    subplot(3,1,3);
    hold on;
    grid;
    error = plot(k(4:n), y(D+1:n) - cell2mat(y_pred), '-r');
    legend(error, 'Error');
    hold off;
end