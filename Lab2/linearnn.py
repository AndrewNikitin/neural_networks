from numpy import vdot
from numpy.random import uniform

class LinearNN:
    """
    Linear Network with delays for predicting time series values.
    """
    def __init__(self, delays=5, learning_rate=0.1, epsilon=1e-6):
        self.epsilon = epsilon
        self.learning_rate = learning_rate
        self.delays = delays
        self.w = None
        self.b = None

    def fit_timeseries(self, train_data, epochs=50, verbose=False):
        self.w = uniform(size=self.delays)
        self.b = 1.0
        current_epoch = 1
        while current_epoch <= epochs:
            acc_error = 0.0
            for i in range(self.delays, train_data.shape[0]):
                prev_values = train_data[i - self.delays:i]
                out = LinearNN.linear_activation(prev_values, self.w, self.b)
                error = train_data[i] - out
                self.w = self.w + self.learning_rate * error * prev_values
                self.b = self.b + self.learning_rate * error
                acc_error += error ** 2
            if verbose and current_epoch % 10 == 0:
                print(f"Performed {current_epoch} epoch(s) out of {epochs}. Accumulative squared error {acc_error}.")
            current_epoch += 1
    
    def fit_filter(self, x, y, epochs=50, verbose=False):
        self.w = uniform(size=self.delays)
        self.b = 1.0
        current_epoch = 1
        while current_epoch <= epochs:
            acc_error = 0.0
            for i in range(self.delays, x.shape[0]):
                prev_values = x[i - self.delays:i]
                out = LinearNN.linear_activation(prev_values, self.w, self.b)
                error = y[i] - out
                self.w = self.w + self.learning_rate * error * prev_values
                self.b = self.b + self.learning_rate * error
                acc_error += error ** 2
            if verbose and current_epoch % 10 == 0:
                print(f"Performed {current_epoch} epoch(s) out of {epochs}. Accumulative squared error {acc_error}.")
            current_epoch += 1

    def predict(self, x):
        return self.linear_activation(x, self.w, self.b)

    @staticmethod
    def linear_activation(x, w, b):
        return vdot(x, w) + b

